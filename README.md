# website
[![Build Status](https://drone.data.coop/api/badges/aalnix/nixaalborg/website/status.svg?ref=refs/heads/main)](https://drone.data.coop/nixaalborg/website)

Run `docker-compose up` or `zola serve --base-url localhost` if you have zola installed.

Built with [Zola](https://getzola.org).

