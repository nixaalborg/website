+++
title = "Hello world!"
description = "Our first post"
+++

# Hello world!

We figured it's time to populate this site a bit. We're planning a meetup sometime soon, no date has been set however. We're also planning to pay [Crypto Aarhus](cryptoaarhus.dk) a visit next month!
